require "core.Point"
require "actors.AnimatedActor"

local miniBgShift = MakePoint(-8, -8)
local captionShift = MakePoint(0, 3)

local function draw(self, shift)
	local screenPos = (self:GetPos() + shift - self.bgSize * 0.5):Floor()
	love.graphics.draw(self.bg, screenPos.x, screenPos.y)
	local miniBgScreenPos = (screenPos + miniBgShift):Floor()
	love.graphics.draw(self.miniBg, miniBgScreenPos.x, miniBgScreenPos.y)

	if not self.text or not self.caption then
		return
	end

	love.graphics.setColor(self.color)
	local textPos = (self:GetPos() + shift - self.textSize * 0.5):Floor()
	love.graphics.draw(self.text, textPos.x, textPos.y, 0)
	love.graphics.setColor(0, 0, 0, 255)
	local captionPos = (miniBgScreenPos + (self.miniBgSize - self.captionSize) * 0.5 + captionShift):Floor()
	love.graphics.draw(self.caption, captionPos.x, captionPos.y)
	love.graphics.setColor(255, 255, 255, 255)
end

local function beginPlay(self)
	local im = self:GetGame():GetInputManager()
	im:SetInputEnabled(false)
	im:BindOnSomethingPressed(self, function()
		self:NextStep()
	end)
end

local function onDestruct(self)
	local im = self:GetGame():GetInputManager()
	im:SetInputEnabled(true)
	im:UnbindObject(self)
    
    if self:GetGame().cutScene then
        self:GetGame():NextLevel()
    end

    if self.name == "frog" then
    	self:GetGame():GetScenario():TriggerEvent("nextLevel")
    end
    
	print("text bubble removed")
end

function nextStep(self)
	if #(self.dialog) >= self.dialogStep then
        if self.dialog[self.dialogStep].sound ~= nil then
            soundManager:PlaySound(self.dialog[self.dialogStep].sound)
        end
        
        if self.dialog[self.dialogStep].pos ~= nil then
            if self.dialog[self.dialogStep].pos == "top" then
                self:SetPos(MakePoint(100, 0))
            elseif self.dialog[self.dialogStep].pos == "bottom" then
                self:SetPos(MakePoint(100, 200))
            elseif self.dialog[self.dialogStep].pos == "hide" then
                self:SetPos(MakePoint(100, 10000))
            end
        end
        
        if self.dialog[self.dialogStep].image ~= nil then
            local params = self.dialog[self.dialogStep]
            params.image = "data/dialogs/" .. self.dialog[self.dialogStep].image
            params.offsetx = -100
            params.offsety = -50
            self.game:GetMainCamera():SetBg(params)
        end
    
		self.text = love.graphics.newText(self.font, self.dialog[self.dialogStep].text)
		self.caption = love.graphics.newText(self.font, self.dialog[self.dialogStep].caption)
		self.color = self.dialog[self.dialogStep].color
		self.dialogStep = self.dialogStep + 1
		self.textSize = MakePoint(self.text:getWidth(), self.text:getHeight())
		self.captionSize = MakePoint(self.caption:getWidth(), self.caption:getHeight())
	else
		self:GetWorld():RemoveActor(self)
	end
end


function MakeTextBubble(game, pos, dialog, name)
	local self = MakeActor(game, pos)
	self:AddTag("text_bubble")
	self:SetDrawFn(draw)
	self:SetOnDestructFn(onDestruct)
	self:SetBeginPlayFn(beginPlay)
	self.font = love.graphics.newFont("data/fonts/PressStart2P.ttf", 8);
	self.bg = love.graphics.newImage("data/textures/dialog_bg.png")
	self.miniBg = love.graphics.newImage("data/textures/dialog_bg_mini.png")
    
    self.game = game
	self.zHeight = 900
	self.name = name

	self.bgSize = MakePoint(self.bg:getWidth(), self.bg:getHeight())
	self.miniBgSize = MakePoint(self.miniBg:getWidth(), self.miniBg:getHeight())

	self.dialog = dialog
	self.dialogStep = 1

	self.NextStep = nextStep

	if self.game.cutScene then
		self:NextStep() -- will be called from input manager
	end

	return self
end
