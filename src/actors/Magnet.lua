require "core.Point"
require "actors.AnimatedActor"

function MakeMagnet(game, pos)
	local self = MakeActor(game, pos)
	self:AddTag("magnet")
	return self
end
