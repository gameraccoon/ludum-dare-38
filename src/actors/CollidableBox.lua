require "core.Point"
require "core.Actor"

local function onCollidedWithHero(self)
	if self.events then
		self:GetGame():GetScenario():TriggerEvents(self.events)
	end
end

function MakeCollidableBox(game, pos, size, events)
	local self = MakeActor(game, pos)
	self:AddTag("collidable")
	self.collisionSize = size

	self.events = events

	self.OnCollidedWithHero = onCollidedWithHero
	return self
end
