require "core.Point"
require "actors.Particle"

local function spawnSwarm(self)
	local world = self:GetWorld()

	for i = 1,self.particlesCount do
		local spawnPos = self:GetPos() + MakePoint(1.0 - math.random() * 2, 1.0 - math.random() * 2) * self.spreadSize
		world:AddActor(MakeParticle(self:GetGame(), spawnPos, self))
	end
end

local function beginPlay(self)
	self:SpawnSwarm()
end

function MakeSwarmSpawner(game, pos, particlesCount, fliesMaxDistance)
	local self = MakeActor(game, pos)
	self:SetBeginPlayFn(beginPlay)
	self:AddTag("swarm_spawner")

	self.particlesCount = particlesCount
	self.spreadSize = 20
	self.fliesMaxDistance = fliesMaxDistance

	self.SpawnSwarm = spawnSwarm

	return self
end
