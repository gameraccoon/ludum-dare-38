require "core.Point"
require "core.Actor"

local anim8 = require "anim8.anim8"

local function setFlip(self, value)
    if (self.flip ~= value) then
        for n,anim in pairs(self.animations) do
            anim:flipH()
        end
        self.flip = value
    end
end

local function playAnimation(self, name)
	self.currentAnimation = self.animations[name]
end

local function playAnimationShifted(self, name)
	self.currentAnimation = self.animations[name]
    self.currentAnimation:gotoFrame(math.random(1, 4))
end

local function update(self, dt)
	if self.currentAnimation then
		self.currentAnimation:update(dt)
	end
end

local function draw(self, shift)
	if not shift then warning("shift is nil"); shift=MakePoint(0,0) end

	local pos = self.pos:Round()	
	if self.currentAnimation then
		self.currentAnimation:draw(
				self.texture,
				shift.x + pos.x,
				shift.y + pos.y,
				0,
				scale,
				scale
			)
	end
end

function MakeAnimatedActor(game, pos, animationFile)
	if not game then warning("game is nil") end
	if not pos then warning("pos is nil") end

	local self = MakeActor(game, pos)
	self:SetUpdateFn(update)
	self:SetDrawFn(draw)

	self.PlayAnimation = playAnimation
    self.PlayAnimationShifted = playAnimationShifted
	
	self.animations = {}
	self.currentAnimation = nil
    self.flip = false
    
    self.SetFlip = setFlip
	
	local animationImportChunk = love.filesystem.load("data/textures/" .. animationFile .. ".lua")
    
    if animationImportChunk == nil then
        return self
    end
       
    local animationImport = animationImportChunk()
	
	self.texture = love.graphics.newImage("data/textures/" .. animationImport.graphic)
	self.frameSize = animationImport.frameSize
	
	-- local tileSize = self.texture:getHeight()
	local grid = anim8.newGrid(self.frameSize[1], self.frameSize[2], self.texture:getWidth(), self.texture:getHeight())
	
	for n,p in pairs(animationImport.animations) do
		local length = p.length
		local gridElements = {}
		
		for i,v in ipairs(p) do
			gridElements[i] = v
		end
		
		self.animations[n] = anim8.newAnimation(grid(gridElements[1], gridElements[2], gridElements[3], gridElements[4], gridElements[5], gridElements[6]), length)
	end

	return self
end
