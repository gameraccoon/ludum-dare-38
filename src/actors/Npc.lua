require "core.Point"
require "actors.AnimatedActor"
require "actors.TextBubble"

local function use(self)
    local dialog = nil

    if self.dialogs then
        for _, nextDialog in pairs(self.dialogs) do
            dialog = nextDialog
            break
        end
        if dialog and not dialog.repeatable then
            table.remove(self.dialogs, 1)
        end
    end

    if dialog then
        self:GetWorld():AddActor(MakeTextBubble(self:GetGame(), self:GetPos() + MakePoint(0, -40), dialog.steps, self.name))
    end

    print("npc id: " .. self.name)
end

function MakeNpc(game, pos, dialogs, name)
	local self = MakeAnimatedActor(game, pos, name)
	self:AddTag("NPC")
    self:AddTag("usable")
	self.collisionSize = MakePoint(16, 16)
    self.collisionOffset = MakePoint(8, 16)
    self.name = name
    
    self:PlayAnimation("idle")

    self.Use = use
    self.dialogs = dialogs

	return self
end
