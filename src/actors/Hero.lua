require "core.Point"
require "actors.AnimatedActor"
require "helpers.CollisionChecker"

local shiftInTile = MakePoint(16, 16)

local function areCollidingNow(self)
    local w = self.collisionSize.x
    local h = self.collisionSize.y
    local x = self.pos.x + self.collisionOffset.x
    local y = self.pos.y + self.collisionOffset.y
    
    local map = self:GetWorld().colliderLayer
    
    return map:IsCollidingPoint(x, y) or map:IsCollidingPoint(x + w, y + h)
        or map:IsCollidingPoint(x + w, y) or map:IsCollidingPoint(x, y + h)
end

local function update(self, dt)
	local world = self:GetWorld()
	local game = self:GetGame()
	local params = game:GetParams()
	local xSpeed = params.heroXSpeed
	local xAccel = params.heroXAccel
	local jumpAccel = params.heroJumpAccel
	local gravityAccel = params.gravityAccel

	local im = game:GetInputManager()
	local xMove = im:GetXInput()
    local yMove = im:GetYInput()

    local oldPos = self:GetPos()

    if not im:IsInputEnabled() then
        xMove = 0
        yMove = 0
    end

	self.speed.x = xMove * xSpeed
	--self.speed.y = yMove * xSpeed
    if self.tryToJump and self.touchingGround then
        self.speed.y = -self:GetGame():GetParams().heroJumpAccel
    else
        self.speed.y = self.speed.y + gravityAccel
    end
    self.tryToJump = false
    
    if self.touchingCeiling and self.speed.y < 0 then
        self.speed.y = 0
    end
	self.touchingCeiling = false

	local collided, newCollided, uncollided = self.collisionChecker:UpdateCollidedObjectsByTag(self, "collidable")	
	
	for actor, _ in pairs(collided) do
		if self.collisionChecker:IsStayingOn(self, actor) then
			if self.speed.y > 0 then
				self.speed.y = 0
			end
			break
		end
	end

    local dx = self.speed.x * dt
    local dy = self.speed.y * dt
    
	--self:SetPos(self:GetPos() + self.speed * dt)
    
    if math.abs(dx) > 0.1 then
    	local iterationsCount = math.ceil(math.abs(dx))
        local stepX = dx / iterationsCount
            
        for x = 1, iterationsCount, 1 do
            self.pos.x = self.pos.x + stepX
            local collide = areCollidingNow(self)
            if collide then
                self.pos.x = self.pos.x - stepX
            end
        end
    end
    
    if math.abs(dy) > 0.1 then
    	local iterationsCount = math.ceil(math.abs(dy))
        local stepY = dy / iterationsCount
        
        self.touchingGround = false
        
        for y = 1, iterationsCount, 1 do
            self.pos.y = self.pos.y + stepY
            local collide = areCollidingNow(self)
            if collide then
                self.pos.y = self.pos.y - stepY
                if dy < 0 then
                    self.touchingCeiling = true
                else
                    self.touchingGround = true
                    self.speed.y = 0
                end
            end
        end
    end

        
    if not self.touchingGround then
        self:PlayAnimation("jump")
    else
        if math.abs(dx) < 0.1 then
            self:PlayAnimation("idle")
        else
            self:PlayAnimation("run")
        end
    end
    
    if math.abs(dx) > 0.1 then
        if (dx > 0) then
            self:SetFlip(false)
        else
            self:SetFlip(true)
        end
    end

	game:GetMainCamera():Follow(self:GetPos() + shiftInTile)

    if oldPos ~= self:GetPos() then
        im:OnMouseMove(MakePoint(0,0))
    end

    for actor in pairs(newCollided) do
        actor:OnCollidedWithHero()
    end

    for actor in pairs(uncollided) do
        --print("collide end")
    end
end

local function onDestruct(self)
	self:GetGame():GetInputManager():UnbindObject(self)
end

-- local function tryToJump(self)
	-- if self.collisionChecker:IsStayingOnSomethingWithTag(self, "collidable") then
    -- if touchingGround then
		-- self.speed.y = self.speed.y - self:GetGame():GetParams().heroJumpAccel
	-- end
-- end

function MakeHero(game, pos)
	local self = MakeAnimatedActor(game, pos, "cat")
	self:SetUpdateFn(update)
	self:AddTag("hero")
	self:SetOnDestructFn(onDestruct)
	self.collisionSize = MakePoint(16, 16)
    self.collisionOffset = MakePoint(8, 16)
    self.touchingGround = false
    self.touchingCeiling = false
    self.tryToJump = false
    
    self:PlayAnimation("idle")

	self.collisionChecker = MakeCollisionChecker()
	self.speed = MakePoint(0, 0)
    
    jumpFunction = function()
            if self:GetGame():GetInputManager():IsInputEnabled() then
    			--tryToJump(self)
                self.tryToJump = true
            end
		end

	self:GetGame():GetInputManager():BindScancode(self, "space", jumpFunction, nil)
    self:GetGame():GetInputManager():BindScancode(self, "w", jumpFunction, nil)
    self:GetGame():GetInputManager():BindScancode(self, "up", jumpFunction, nil)

    self:GetGame():GetInputManager():BindScancode(self, "e",
        function()
            if self:GetGame():GetInputManager():IsInputEnabled() then
                local collided = self.collisionChecker:GetCollidedObjectsByTag(self, "usable")
                for actor in pairs(collided) do
                    actor:Use()
                    break -- only one
                end
            end
        end,
        nil)


	return self
end
