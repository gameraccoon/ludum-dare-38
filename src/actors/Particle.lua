require "core.Point"
require "actors.AnimatedActor"

local function magic(self)
    local w = self.collisionSize.x
    local h = self.collisionSize.y
    local x = self.pos.x + self.collisionOffset.x
    local y = self.pos.y + self.collisionOffset.y
    
    local collide = false
    
    local hiddenLayers = self:GetWorld():GetAllActorsWithTag("hidden")
    --print("hidden layers: " .. #hiddenLayers)
	for map in pairs(hiddenLayers) do
        collide = map:IsCollidingPoint(x, y) or map:IsCollidingPoint(x + w, y + h)
            or map:IsCollidingPoint(x + w, y) or map:IsCollidingPoint(x, y + h)
            
        if collide then
            --error(1)
            map:Unhide()
        end
    end
    
    --return collide
end

local function update(self, dt)
	local world = self:GetWorld()
	local game = self:GetGame()
	local params = game:GetParams()
	local maxSpeed = params.particleSpeed
	local maxStandoffSize = params.maxStandoffSize
	local standoffPower = params.standoffPower
	local magnetMaxDistance = params.magnetMaxDistance
	
--============= movement
	local speed = MakePoint(0, 0)

	local target = nil

	if self.spawner and self.spawner:IsValid() then
		target = self.spawner
	elseif self.spawner and not self.spawner:IsValid() then
		-- remove reference to let GC remove object
		self.spawner = nil
	end

	local maxSpawnerDistance = nil
	local spawnPos = nil
	if self.spawner then
		maxSpawnerDistance = self.spawner.fliesMaxDistance
		spawnPos = self.spawner:GetPos()
	end

	local magnets = world:GetAllActorsWithTag("magnet")
	local nearestMagnetDistance = magnetMaxDistance
	for magnet in pairs(magnets) do
		local distance = (magnet:GetPos() - self:GetPos()):Size()
		if distance < nearestMagnetDistance then
			if not maxSpawnerDistance or (magnet:GetPos() - spawnPos):Size() < maxSpawnerDistance then
				target = magnet
				nearestMagnetDistance = distance
			end
		end
	end
	
	if target then
		local delta = target:GetPos() - self:GetPos()
		speed = speed + delta:Ort()
	end

	speed = speed:Ort() * maxSpeed

	local standoffSpeed = MakePoint(0, 0)
	local particles = world:GetAllActorsWithTag("particle")
	for particle in pairs(particles) do
		if particle ~= self then
			local distance = (particle:GetPos() - self:GetPos()):Size()
			if distance < maxStandoffSize then
				if distance == 0 then
					distance = 0.001
				end

				standoffSpeed = - (particle:GetPos() - self:GetPos()):Ort() / distance * standoffPower
			end
		end
	end
    
    magic(self)

	self:SetPos(self:GetPos() + speed + standoffSpeed)
end

function MakeParticle(game, pos, spawner)
	local self = MakeAnimatedActor(game, pos, "particle")
	self:SetUpdateFn(update)
	self:AddTag("particle")
	self.zHeight = spawner.zHeight
    self.collisionSize = MakePoint(32, 32)
    self.collisionOffset = MakePoint(0, 0)
    
    self:PlayAnimationShifted("idle")

    self.spawner = spawner

    self.SetPos = function(self, pos)
    	self.pos = pos - MakePoint(16, 16)
    end

    self.GetPos = function(self)
    	return self.pos + MakePoint(16, 16)
    end

	return self
end
