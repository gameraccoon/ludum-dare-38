local CollisionChecker = {}
CollisionChecker.__index = CollisionChecker

function MakeCollisionChecker()
	local self = setmetatable({}, CollisionChecker)
	self.collidedObjects = {}
	return self
end

function CollisionChecker:GetCollidedObjectsByTag(actor, tag)
	local collided = {}
	local collidable = actor:GetWorld():GetAllActorsWithTag(tag)
	for worldActor in pairs(collidable) do
		if worldActor ~= actor and self:IsCollided(actor, worldActor) then
			collided[worldActor] = true
		end
	end
	return collided
end

function CollisionChecker:IsStayingOnRect(actor, a2LT, a2Size)
	local a1Pos = actor:GetPos()
	local a1Size = actor:GetCollisionSize()

	local a1LT = a1Pos - a1Size * 0.5
	local a1BD = a1Pos + a1Size * 0.5
	local a2BD = a2LT + a2Size

	if a1LT.x > a2BD.x or a1LT.y > a2BD.y or
		a2LT.x > a1BD.x or a2BD.y > a1BD.y then
		return false
	end

	return true
end

function CollisionChecker:IsStayingOn(actor1, actor2)
	if not actor1 then warning("actor1 is nil"); return false end
	if not actor2 then warning("actor2 is nil"); return false end
	
	local a2Pos = actor2:GetPos()
	local a2Size = actor2:GetCollisionSize()

	local a2LT = a2Pos - a2Size * 0.5

	return self:IsStayingOnRect(actor1, a2LT, a2Size)
end

local function setDifference(a,b)
	local res = {}
	for k in pairs(a) do
		if b[k] == nil then
			res[k] = true
		end
	end
	return res
end

local function tablelength(t)
	local count = 0
	for _ in pairs(t) do count = count + 1 end
	return count
end

function CollisionChecker:UpdateCollidedObjectsByTag(actor, tag)
	local collided = self:GetCollidedObjectsByTag(actor, tag)
	local newCollided = setDifference(collided, self.collidedObjects)
	local uncollided = setDifference(self.collidedObjects, collided)
	self.collidedObjects = collided

	return collided, newCollided, uncollided
end

function CollisionChecker:IsCollided(actor1, actor2)
	if not actor1 then warning("actor1 is nil"); return false end
	if not actor2 then warning("actor2 is nil"); return false end

	local a1Pos = actor1:GetPos()
	local a1Size = actor1:GetCollisionSize()

	local a2Pos = actor2:GetPos()
	local a2Size = actor2:GetCollisionSize()

	local a1LT = a1Pos + actor1.collisionOffset
	local a1BD = a1LT + a1Size
	local a2LT = a2Pos + actor2.collisionOffset
	local a2BD = a2LT + a2Size

	if a1LT.x > a2BD.x or a1LT.y > a2BD.y or
		a2LT.x > a1BD.x or a2LT.y > a1BD.y then
		return false
	end

	return true
end

function CollisionChecker:IsStayingOnSomethingWithTag(actor, tag)
	local collided = self:GetCollidedObjectsByTag(actor, tag)
	for collidedActor in pairs(collided) do
		if self:IsStayingOn(actor, collidedActor) then
			return true
		end
	end
	return false
end


-------------------------------------------------------
---- Tests
-------------------------------------------------------
--[[
AddTest("CollisionChecker-Collide", function(checker)
	local world = MakeWorld()
	local game = MakeGame(world, MakePoint(10, 10))

	local actor1 = MakeCollidableBox(game, MakePoint(0, 0), MakePoint(100, 100))
	world:AddActor(actor1)
	local actor2 = MakeCollidableBox(game, MakePoint(0, 0), MakePoint(100, 100))
	world:AddActor(actor2)

	local collision = MakeCollisionChecker()

	checker:TestTrue(collision:IsCollided(actor1, actor2))

	local collided, new, old = collision:UpdateCollidedObjectsByTag(actor1, "collidable")
	checker:TestFalse(collided[actor1])
	checker:TestTrue(collided[actor2])
	checker:TestFalse(new[actor1])
	checker:TestTrue(new[actor2])
	checker:TestFalse(old[actor1])
	checker:TestFalse(old[actor2])

	actor2:SetPos(MakePoint(40, 40))
	checker:TestTrue(collision:IsCollided(actor1, actor2))
	collided, new, old = collision:UpdateCollidedObjectsByTag(actor1, "collidable")
	checker:TestFalse(collided[actor1])
	checker:TestTrue(collided[actor2])
	checker:TestFalse(new[actor1])
	checker:TestFalse(new[actor2])
	checker:TestFalse(old[actor1])
	checker:TestFalse(old[actor2])

	actor1:SetPos(MakePoint(-100, 40))
	checker:TestFalse(collision:IsCollided(actor1, actor2))
	collided, new, old = collision:UpdateCollidedObjectsByTag(actor1, "collidable")
	checker:TestFalse(collided[actor1])
	checker:TestFalse(collided[actor2])
	checker:TestFalse(new[actor1])
	checker:TestFalse(new[actor2])
	checker:TestFalse(old[actor1])
	checker:TestTrue(old[actor2])


	actor1:SetPos(MakePoint(-100, 0))
	actor2:SetPos(MakePoint(-10, 0))
	checker:TestTrue(collision:IsCollided(actor1, actor2))

	actor1:SetPos(MakePoint(10, 190))
	actor2:SetPos(MakePoint(1, 0))
	checker:TestFalse(collision:IsCollided(actor1, actor2))
end)

AddTest("CollisionChecker-IsStayingOn", function(checker)
	local world = MakeWorld()
	local game = MakeGame(world, MakePoint(10, 10))

	local hero = MakeCollidableBox(game, MakePoint(0, 0), MakePoint(300, 30))
	world:AddActor(hero)
	local platform = MakeCollidableBox(game, MakePoint(0, 0), MakePoint(30, 30))
	world:AddActor(platform)

	local collision = MakeCollisionChecker()

	hero:SetPos(MakePoint(-1, 31))
	platform:SetPos(MakePoint(10, 2))
	checker:TestTrue(collision:IsStayingOn(hero, platform))

	hero:SetPos(MakePoint(10, 90))
	platform:SetPos(MakePoint(3, 2))
	checker:TestFalse(collision:IsStayingOn(hero, platform))

	hero:SetPos(MakePoint(450, 13))
	platform:SetPos(MakePoint(3, 2))
	checker:TestFalse(collision:IsStayingOn(hero, platform))

	hero:SetPos(MakePoint(10, 10))
	platform:SetPos(MakePoint(3, 12))
	checker:TestFalse(collision:IsStayingOn(hero, platform))
end)
--]]
