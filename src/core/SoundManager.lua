local SoundManager = {}
SoundManager.__index = SoundManager

function MakeSoundManager()
	local self = setmetatable({}, SoundManager)
	self.sounds = {}
	return self
end

function SoundManager:GetSound(name)
	local s = self.sounds[name]
	if type(s) == "table" then
		local source = s[math.random(#s)]
		--love.audio.play(source)
		return source
	else
		--love.audio.play(s)
		return s
	end
end

function SoundManager:PlaySound(name)
	local sound = self:GetSound(name)
	love.audio.play(sound)
	return sound
end

function SoundManager:PlaySoundIndependent(name)
    local sound = self:GetSound(name):clone()
	love.audio.play(sound)
	return sound
end

function SoundManager:CloneSound(name)
	return self.GetSound(name):clone()
end

function SoundManager:StopSound(name)
	local s = self.sounds[name]
	if type(s) == "table" then
		for i,p in ipairs(s) do
			love.audio.stop(p)
		end
	else
		love.audio.stop(s)
	end
end

function SoundManager:LoadAll()
	math.randomseed( os.time() )
	
	-- TODO:
	-- ? sound looping with a few different sounds (footsteps)

    local soundsList = love.filesystem.load("data/sound/list.lua")()
	
	for name,path in pairs(soundsList) do
		if type(path) == "table" then
			-- if several paths and/or parameters
			local list = {}
			local volume
			local loop = false
			for n,p in pairs(path) do
				if (n == "volume") then
					volume = p
				elseif (n == "loop") then
					loop = p
				else
					table.insert(list, love.audio.newSource("data/sound/" .. p, "static"))
				end
			end
			
			-- set up parameters
			for i,p in ipairs(path) do
				list[i]:setLooping(loop)
				if type(volume) == "table" then
					list[i]:setVolume(volume[i])
				else
					list[i]:setVolume(volume)
				end
			end
			self.sounds[name] = list
		else
			-- if just name and path
			self.sounds[name] = love.audio.newSource("data/sound/" .. path, "static")
		end
	end
	
	--local sound = love.audio.newSource("data/sound/hostile_creature_detected.wav")
	--self.sounds["abc"] = sound
end