require "utils.Tests"
require "utils.Debug"
require "core.Camera"
require "core.World"
require "core.InputManager"
require "core.Tilemap"
require "core.ScenarioManager"

require "actors.SwarmSpawner"
require "actors.CollidableBox"
require "actors.Magnet"
require "actors.Hero"
require "actors.TextBubble"
require "actors.Npc"

local Game = {}
Game.__index = Game

function MakeGame(canvasSize)
	local self = setmetatable({}, Game)
	self.world = MakeWorld()
	self.canvasSize = canvasSize
	self.camera = MakeCamera(self.world, self.canvasSize)
	self.input = MakeInputManager()
	self.params = {}
	self.magnet = nil
	self.hero = nil
	self.currentLevel = "intro_cat"
	self.scenario = MakeScenarioManager(self, self.currentLevel)
	return self
end

function Game:NextLevel()
    if self.currentLevel == "intro_cat" then
        self:ChangeLevel("level1")	
    elseif self.currentLevel == "level1" then
        self:ChangeLevel("level2")	
    elseif self.currentLevel == "level2" then
        self:ChangeLevel("king_cat")	
    elseif self.currentLevel == "king_cat" then
        self:ChangeLevel("level3")	
    elseif self.currentLevel == "level3" then
        self:ChangeLevel("frog_cat")	
    elseif self.currentLevel == "frog_cat" then
        self:ChangeLevel("credits")
    elseif self.currentLevel == "credits" then
        self:ChangeLevel("credits")	
    end
end

function Game:GetMainCamera()
	return self.camera
end

function Game:GetWorld()
	return self.world
end

function Game:GetParams()
	return self.params
end

function Game:GetInputManager()
	return self.input
end

function Game:GetScenario()
	return self.scenario
end

function Game:Update(dt)
	if self.input then
		self.input:Update(dt)
	else
		warning("sels.input is nil")
	end

	if self.world then
		self.world:ForeachActor(function(actor)
			actor:Update(dt)
		end)
	else
		warning("world is nil")
	end
end

function Game:Init()
	self.params.particleSize = 4
	self.params.particleDrawSize = 4
	self.params.particleSpeed = 1
	self.params.maxStandoffSize = 10
	self.params.standoffPower = 20
	self.params.heroXSpeed = 65 -- 100
	self.params.heroXAccel = 1
	self.params.heroJumpAccel = 155 -- 270
	self.params.gravityAccel = 3
	self.params.magnetMaxDistance = 100
	self.params.particlesMaxDistance = 200

	self.input:BindOnMouseDown(self, function()
        if self:GetInputManager():IsInputEnabled() then
			local worldPos = self.camera:ScreenToWorld(self.input:GetMousePos())

			if self.magnet and self.magnet:IsValid() then
				self:GetWorld():RemoveActor(self.magnet)
				self.magnet = nil
			end

			self.magnet = MakeMagnet(self, worldPos)
			self.world:AddActor(self.magnet)

		end
	end)

	self.input:BindOnMouseMove(self, function(delta)
        if self:GetInputManager():IsInputEnabled() then
			local worldPos = self.camera:ScreenToWorld(self.input:GetMousePos())

			if self.magnet then
				self.magnet:SetPos(worldPos)
                
                soundManager:PlaySound("attract")
			end
		end
	end)

	self.input:BindOnMouseUp(self, function()
		if self.magnet and self.magnet:IsValid() then
			self:GetWorld():RemoveActor(self.magnet)
			self.magnet = nil
            
            soundManager:StopSound("attract")
		end
	end)

	self.input:BindScancode(self, "2", function()
		SetUpscale(2)
	end, nil)
	self.input:BindScancode(self, "3", function()
		SetUpscale(3)
	end, nil)
	self.input:BindScancode(self, "4", function()
		SetUpscale(4)
	end, nil)
end

function Game:ChangeLevel(level)
	--self.currentLevel = "level2"
	self.world = MakeWorld()
	self.camera = MakeCamera(self.world, self.canvasSize)
	self.magnet = nil
	self.hero = nil
	self.currentLevel = level
	self.scenario = MakeScenarioManager(self, self.currentLevel)

	self:StartGame()
end

function Game:StartGame()
    
    self.camera:SetPos(MakePoint(100,100))

    local mapChunk = love.filesystem.load("data/map/" .. self.currentLevel ..  ".lua")

    local dialogs = love.filesystem.load("data/dialogs/dialogs.lua")()
    
    self.cutScene = false
    
    if mapChunk ~= nil then
    
        local map = mapChunk()
        
        local tileset = map.tilesets[1]
        local texture = love.graphics.newImage("data/map/" .. tileset.image)
        --local texture = love.graphics.newImage("data/textures/tileset.png")
        
        local hiddenLayers = {}
        
        local z = -100
        
        soundManager:StopSound("track2")
        
        for i, v in ipairs(map.layers) do
            if v.type == "objectgroup" then
                for i, v in pairs(v.objects) do
                    if v.type == "spawn" then
                        --self.world:AddActor(MakeSpike(self, MakePoint(v.x, 16)))
                        self.hero = MakeHero(self, MakePoint(v.x, v.y))
                        self:GetMainCamera():Follow(self.hero:GetPos())
                        print("obj Z: " .. v.type .. " -> " .. z)
                        
                        self.hero.zHeight = z
                        self.world:AddActor(self.hero)

                    elseif v.type == "fly" then 
                        local maxDistance = v.properties.distance
                        if maxDistance == nil then
                            maxDistance = self.params.particlesMaxDistance
                        end
                        
                        local spawner = MakeSwarmSpawner(self, MakePoint(v.x + 16, v.y + 16), 5, maxDistance)
                        spawner.zHeight = z
                        self.world:AddActor(spawner)

                        

                        --local portal = MakeCollidableBox(self, MakePoint(v.x + 100, v.y), MakePoint(20, 20), {"nextLevel"})
                        --self.world:AddActor(portal)

                    elseif v.type == "npc" then
                        local baran = MakeNpc(self, MakePoint(v.x, v.y), dialogs[self.currentLevel .. "_" .. v.name], v.name)
                        baran.zHeight = z
                        self.world:AddActor(baran)
                        
                    elseif v.type == "death" then
                        local d = MakeCollidableBox(self, MakePoint(v.x, v.y), MakePoint(v.width, v.height), {"restartLevel"})
                        --baran.zHeight = z
                        self.world:AddActor(d)
                        
                    elseif v.type == "finish" then
                        local d = MakeCollidableBox(self, MakePoint(v.x, v.y), MakePoint(v.width, v.height), {"nextLevel"})
                        --baran.zHeight = z
                        self.world:AddActor(d)
                        
                    end
                end
            elseif v.type == "tilelayer" then
                local isCollider = (v.name == "collide")
                local isHidden = v.properties.hidden ~= nil
                
                local tilemap = MakeTilemap(self, MakePoint(v.x, v.y), isCollider, isHidden, 5)
                if isCollider then
                    colliderLayer = tilemap
                end
                if isHidden then
                    table.insert(hiddenLayers, tilemap)
                end
                tilemap:Load(v.data, v.width, v.height, texture, tileset.tilewidth, tileset.tileheight)
                
                print("layer Z: " .. v.name .. " -> " .. z)
                
                tilemap.zHeight = z
                

                self.world:AddActor(tilemap)
            elseif v.type == "imagelayer" and v.visible then
                v.image = "data/map/" .. v.image
                self.camera:SetBg(v)
            end
            
            z = z + 10
        end
        
        love.graphics.setBackgroundColor(map.backgroundcolor)
        
        -- in a separate loop because hidden layer can go before the collider one, which we pass to the function
        for i, v in ipairs(hiddenLayers) do
            v:SetColliderTilemap(colliderLayer)
            --v:Unhide()
        end
        
        self.world.colliderLayer = colliderLayer
        
        soundManager:PlaySound("track1")
    else
        soundManager:StopSound("track1")
        soundManager:StopSound("track2")
        
        self.hero = MakeHero(self, MakePoint(0, 0))

        local baran = MakeNpc(self, MakePoint(0, 0), dialogs[self.currentLevel], self.currentLevel)
        
        --self.world:AddActor(self.hero)
        self.world:AddActor(baran)
        --self.world:AddActor(baran)
        
        self.cutScene = true
        
        baran:Use()
    end


    -- cheat start first dialog
    if self.currentLevel == "level1" then
    	local npcs = self:GetWorld():GetAllActorsWithTag("NPC")
    	for npc in pairs(npcs) do
    		if npc.name == "npc_3_sw" then
    			self.cutScene = true
    			npc:Use()
    			self.cutScene = false

				local bubbles = self:GetWorld():GetAllActorsWithTag("text_bubble")
    			for bubble in pairs(bubbles) do
    				bubble:SetPos(self:GetMainCamera().pos + MakePoint(0,-10)) 
				end
    			break
    		end
    	end
    end
end
