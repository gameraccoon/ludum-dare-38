require "utils.Tests"
require "utils.Debug"


local Point = {}
Point.__index = Point

function MakePoint(x, y)
	if not x then x = 0; warning("x is nil") end
	if not y then y = 0; warning("y is nil") end

	local self = setmetatable({}, Point)
	self.x = x
	self.y = y

	return self
end

function Point:Clone()
	return MakePoint(self.x, self.y)
end

function Point:Size()
	return math.sqrt(self.x * self.x + self.y * self.y)
end

function Point:Ort()
	local len = self:Size()
	if size ~= 0.0 then
		return MakePoint(self.x / len, self.y / len)
	else
		return MakePoint(0.0, 0.0)
	end
end

function Point:Floor()
	return MakePoint(math.floor(self.x), math.floor(self.y))
end

local function round(x)
	if x%2 ~= 0.5 then
		return math.floor(x + 0.5)
	else
		return x - 0.5
	end
end

function Point:Round()
	return MakePoint(round(self.x), round(self.y))
end

function Point.__eq(lhs, rhs)
	return lhs.x == rhs.x and lhs.y == rhs.y
end

function Point.__add(lhs, rhs)
	return MakePoint(lhs.x + rhs.x, lhs.y + rhs.y)
end

function Point.__sub(lhs, rhs)
	return MakePoint(lhs.x - rhs.x, lhs.y - rhs.y)
end

function Point.__unm(point)
	return MakePoint(-point.x, -point.y)
end

function Point.__mul(lhs, rhs)
	if type(lhs) == "number" then
		return MakePoint(rhs.x * lhs, rhs.y * lhs)
	else
		return MakePoint(lhs.x * rhs, lhs.y * rhs)
	end
end

function Point.__div(lhs, rhs)
	return MakePoint(lhs.x / rhs, lhs.y / rhs)
end

-------------------------------------------------------
---- Tests
-------------------------------------------------------

AddTest("Point-Create", function(checker)
	local point = MakePoint(3, -2)

	checker:TestEqual(3, point.x)
	checker:TestEqual(-2, point.y)
end)

AddTest("Point-Multiply", function(checker)
	local point1 = MakePoint(3, -2) * 0.5
	local point2 = 2 * MakePoint(1, 0)

	checker:TestEqualFloat(1.5, point1.x)
	checker:TestEqualFloat(-1, point1.y)

	checker:TestEqualFloat(2, point2.x)
	checker:TestEqualFloat(0, point2.y)
end)

AddTest("Point-UnaryMinus", function(checker)
	local point1 = -MakePoint(3, -2)
	local point2 = MakePoint(-10, 5)

	checker:TestTrue(MakePoint(-3, 2) == point1)
	checker:TestTrue(MakePoint(10, -5) == -point2)
end)

AddTest("Point-Equality", function(checker)
	local point1 = MakePoint(3, -2)
	local point2 = MakePoint(3, -2)
	local point3 = MakePoint(3, 2)
	local point4 = MakePoint(3, 0)
	local point5 = MakePoint(0, -2)
	local point6 = MakePoint(-2, -3)

	checker:TestTrue(point1 == point2)
	checker:TestFalse(point1 == point3)
	checker:TestFalse(point1 == point4)
	checker:TestFalse(point1 == point5)
	checker:TestFalse(point1 == point6)
	
	checker:TestFalse(point1 ~= point2)
	checker:TestTrue(point1 ~= point3)
end)

AddTest("Point-AddSubstract", function(checker)
	local point1 = MakePoint(3, -2)
	local point2 = MakePoint(-2, -4)

	local sumPont = point1 + point2
	local diffPont = point1 - point2

	checker:TestTrue(sumPont == MakePoint(1, -6))
	checker:TestTrue(diffPont == MakePoint(5, 2))
end)

AddTest("Point-Clone", function(checker)
	local point1 = MakePoint(3, -2)
	local point2 = point1
	local point3 = point1:Clone()

	point1.y = 2

	checker:TestEqual(2, point2.y)
	checker:TestEqual(-2, point3.y)
end)
