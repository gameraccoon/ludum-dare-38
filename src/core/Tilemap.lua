require "core.Point"
require "core.Actor"
require "actors.CollidableBox"
      
local shader = love.graphics.newShader[[
		extern vec4 tint;
		extern number strength;
		vec4 effect(vec4 color, Image texture, vec2 tc, vec2 _)
		{
			color = Texel(texture, tc);
			//number luma = dot(vec3(0.299f, 0.587f, 0.114f), color.rgb);
			//return mix(color, tint * luma, strength);
            return color * vec4(1, 0.7, 0, 0.5);
		}
        ]]
        
--shader:send("tint",{1.0,1.0,1.0,1.0})
--shader:send("strength",0.5)

local function draw(self, shift)
    local highlight = self:GetGame():GetInputManager():IsMouseDown()
    if not self.hidden or highlight then
        --local shader = love.graphics.getShader( )
        if (self:HasTag("hidden")) and highlight and self.hidden then
            love.graphics.setShader( shader )
        end

        pos = self.pos:Round()
        
        love.graphics.draw(self.spriteBatch, pos.x + shift.x, pos.y + shift.y)
        
        love.graphics.setShader(  )
    end
end

local function setTile(self, x, y, tileID)
    self:setTileByIndex(x + y * self.widthInTile + 1, tileID)
end

local function setTileByIndex(self, index, tileID)
    self.tileArray[index] = tileID
    
    if self.quadMap[index] ~= nil  then
        self.quadMap[index] = nil
    end
    
    if (tileID > 0) then
        tileID = tileID - 1
        
        local x = math.floor((index - 1) % self.widthInTiles)
        local y = math.floor((index - 1) / self.widthInTiles)
        
        local tileset_widthInTiles = math.floor(self.texture:getWidth() / self.tileWidth)
        local tileset_heightInTiles = math.floor(self.texture:getHeight() / self.tileHeight)
        local tilesetX = math.floor(tileID % tileset_widthInTiles)
        local tilesetY = math.floor(tileID / tileset_widthInTiles)
        self.quadMap[index] = love.graphics.newQuad(self.tileWidth * tilesetX, self.tileHeight * tilesetY, self.tileWidth, self.tileHeight, self.texture:getDimensions())
            
        self.spriteBatch:add(self.quadMap[index], x * self.tileWidth, y * self.tileHeight)
    else
        self.quadMap[index] = nil
    end
end

local function load(self, tileArray, widthInTiles, heightInTiles, texture, tileWidth, tileHeight)
    self.tileArray = tileArray
    self.quadMap = {}
    self.texture = texture
    self.widthInTiles = widthInTiles
    self.heightInTiles = heightInTiles
    self.tileWidth = tileWidth
    self.tileHeight = tileHeight
    
    local tileCount = widthInTiles * heightInTiles
    self.spriteBatch = love.graphics.newSpriteBatch(self.texture, tileCount)
    for i = 1, tileCount, 1 do
        setTileByIndex(self, i, tileArray[i])
    end
end

local function getTile(self, x, y)
    return self.tileArray[x + y * self.widthInTiles + 1]
end

local function isTileCollider(self, x, y)
	return self:GetTile(x, y) ~= 0
end

local function isCollidingPoint(self, x, y)
    if ((x < self.pos.x) or (y < self.pos.y) or (x >= self.pos.x + self.widthInTiles * self.tileWidth) or (y >= self.pos.y + self.heightInTiles * self.tileHeight)) then
        -- for flies - false, for cat - true
        return self:GetWorld().colliderLayer == self
    end
	return self:IsTileCollider(math.floor(x / self.tileWidth), math.floor(y / self.tileHeight))
end

local function setColliderTilemap(self, colliderTilemap)
    self.colliderTilemap = colliderTilemap
end

local function unhide(self)
    self.timeLimit = self.timeLimitInit
        
    if self.hidden then
        self.hidden = false
        
        for i = 1, #self.colliderTilemap.tileArray, 1 do
            if self.tileArray[i] > 0 then
                setTileByIndex(self.colliderTilemap, i, self.tileArray[i])
            end
        end
             
        soundManager:PlaySound("magic")
    end
end

local function hide(self)
    if not self.hidden then
        self.hidden = true
        
        self.timeLimit = nil
        
        for i = 1, #self.colliderTilemap.tileArray, 1 do
            if self.tileArray[i] > 0 then
                setTileByIndex(self.colliderTilemap, i, 0)
            end
        end
        
        soundManager:PlaySound("unmagic")
    end
end

local function update(self, dt)
    if self.timeLimit ~= nil then
        self.timeLimit = self.timeLimit - dt
        if (self.timeLimit < 0) then
            hide(self)
        end

    end
end

function MakeTilemap(game, pos, collider, hidden, timeLimitInit)
	if not game then warning("game is nil") end
	if not pos then warning("pos is nil") end

    local self
    -- if collider then
        -- self = MakeCollidable(game, pos)
    -- else
        self = MakeActor(game, pos)
    -- end
    
    self.hidden = hidden
    
    if hidden then
        self:AddTag("hidden")

    end
    
    if not collider then
        self:SetDrawFn(draw)
    end
    
    self:SetUpdateFn(update)

    self.tileArray = nil
    self.texture = nil
    self.widthInTiles = 0
    self.heightInTiles = 0
    self.timeLimit = nil
    self.timeLimitInit = timeLimitInit
    
    self.IsTileCollider = isTileCollider
    self.IsCollidingPoint = isCollidingPoint
    self.GetTile = getTile
    self.SetTile = setTile
    self.Load = load
    self.SetColliderTilemap = setColliderTilemap
    self.Unhide = unhide
    self.Hide = hide
    
    self.spriteBatch = nil
	return self
end
