require "utils.Tests"
require "utils.Debug"
require "core.Point"

local Camera = {}
Camera.__index = Camera

function MakeCamera(world, canvasSize)
	if not world then warning("world is nil") end

	local self = setmetatable({}, Camera)
	self.world = world
	self.pos = MakePoint(0, -100)
	self.frameSize = 20
	self.bgTexture = nil
	self.bgData = nil
	self.canvas = love.graphics.newCanvas(canvasSize.x, canvasSize.y)
	self.canvasSize = canvasSize
	self.cameraShift = MakePoint(0, -20)
	return self
end

function Camera:SetPos(pos)
	self.pos = pos
end

function Camera:Shift(vector)
	self.pos = self.pos + vector
end

function Camera:GetViewportSize()
	return MakePoint(love.graphics.getWidth(), love.graphics.getHeight())
end

function Camera:Follow(inPos)
	local colliderLayer = self.world.colliderLayer
	local pos = inPos + self.cameraShift

	if colliderLayer and colliderLayer:IsValid() then
		local gridLT = colliderLayer.pos
		local gridSize = MakePoint(colliderLayer.widthInTiles * colliderLayer.tileWidth, colliderLayer.heightInTiles * colliderLayer.tileHeight)
		local gridRB = gridLT + gridSize

		local camLT = pos - self.canvasSize * 0.5
		local camRB = pos + self.canvasSize * 0.5

		if camLT.x < gridLT.x then
			pos.x = pos.x + (gridLT.x - camLT.x)
		elseif camRB.x > gridRB.x then
			pos.x = pos.x + (gridRB.x - camRB.x)
		end

		if camLT.y < gridLT.y then
			pos.y = pos.y + (gridLT.y - camLT.y)
		elseif camRB.y > gridRB.y then
			pos.y = pos.y + (gridRB.y - camRB.y)
		end

		self:SetPos(pos)
	else
		self:SetPos(pos)
	end
end

function Camera:ScreenToWorld(pos)
	local centerPos = pos - self:GetViewportSize() * 0.5
	centerPos = centerPos * 1 / GetUpscale()
	return self.pos + centerPos	
end

function Camera:SetBg(bgData)
	self.bgData = bgData
	self.bgTexture = love.graphics.newImage(bgData.image)
end

function Camera:DrawBackgrounds()
	if self.bgTexture and self.bgData then
		local shift = (self.canvasSize * 0.5 - self.pos):Round()
	    love.graphics.draw(self.bgTexture, self.bgData.offsetx + shift.x, self.bgData.offsety + shift.y)
	end
end

function Camera:Draw()
	love.graphics.setCanvas(self.canvas)
    
    --love.graphics.setBackgroundColor(0, 0, 0)
	love.graphics.clear()
	love.graphics.setBlendMode("alpha")
	--love.graphics.setColor(255, 255, 255, 255)
    self:DrawBackgrounds()
    
	local shift = (-self.pos + self.canvasSize * 0.5):Round()
	self.world:ForeachActorInDrawOrder(function(actor)
		actor:Draw(shift)
	end)
	love.graphics.setCanvas()
	love.graphics.setBlendMode("alpha", "premultiplied")
	love.graphics.draw(self.canvas, 0, 0, 0, love.graphics.getWidth() / self.canvasSize.x, love.graphics.getHeight() / self.canvasSize.y)
end


-------------------------------------------------------
---- Tests
-------------------------------------------------------

