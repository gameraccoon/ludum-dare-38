require "utils.Tests"
require "utils.Debug"


local ScenarioManager = {}
ScenarioManager.__index = ScenarioManager

function MakeScenarioManager(game, level)
	if not game then warning("game is nil") end
	if not level then warning("level is nil") end

	local self = setmetatable({}, ScenarioManager)
	self.triggeredEvents = {}

	self.game = game

	return self
end

function ScenarioManager:TriggerEvents(events)
	for _, event in pairs(events) do
		self:TriggerEvent(event)
	end
end

function ScenarioManager:WasTriggered(event)
	return self.triggeredEvents[event] ~= nil
end

function ScenarioManager:TriggerEvent(event)
	if event == "nextLevel" then
        self.game:NextLevel()
    elseif event == "restartLevel" then
        soundManager:PlaySound("cat_fall_void")
        self.game:ChangeLevel(self.game.currentLevel)	
	end



	self.triggeredEvents[event] = true
end
