return {
track1={"ld mix 01.ogg", volume=1, loop=true},
track2={"ld boss mix 01.ogg", volume=1, loop=true},

crowd={"crowd sfx.wav", volume=1},
sphere={"sphere sound.wav", volume=1},
attract={"attract.wav", volume=0.25},

jump={"jump1.wav", volume=1},
cat_hurt={"cat_3.wav", volume=1},
cat_fight={"cat_6.wav", volume=1},
fight_end={"undo2.wav", volume=1},
magic2={"magic2.wav", volume=1},
frog={"evil frog sfx 2.wav", volume=1},

cat_into_sphere={"cat_9.wav", volume=1},
cat_fall_void={"cat_11.wav", volume=1},

magic={"magic1.wav", "magic2.wav", "magic3.wav", volume=0.3},
unmagic={"undo1.wav", "undo2.wav", "undo3.wav", volume=0.3}

}