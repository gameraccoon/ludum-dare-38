return {
graphic="particle.png",
frameSize= { 32, 32 },

animations =
    {
        idle = {'1-4', 1, length=0.2 },
        death = {'5-5', 1, length=0.5 }
    }
}