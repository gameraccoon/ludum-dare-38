return {
graphic="persi.png",
frameSize= { 32, 32 },

animations =
    {
        idle = {'1-1', 6, '6-6', 6, length=1 / (130/60) },
		run = {'1-1', 6, '3-3', 6,'1-1', 6,'4-4', 6,length=1 / 5  },
		dialog = {'1-1', 6, length=1 / 5 },
		attack = {'2-2',6, length=1 / 5 },
		
        jump = {'5-5', 6, length=0.5 }
    }
}