return {
graphic="persi.png",
frameSize= { 32, 32 },

animations =
    {
        idle = {'1-2', 1, length=1 / (130/60) },
		run = {'2-5', 1, length=1 / 5  },
		dialog = {'6-6', 1, length=1 / 5 },
		act = {'7-8', 1,'8-8',1,'8-8',1,'7-7',1, length=1 / 5 },
		
        jump = {'3-3', 1, length=0.5 }
    }
}