return {
graphic="persi.png",
frameSize= { 32, 32 },

animations =
    {
        idle = {'1-2', 2, length=1 / (130/60) },
		run = {'3-3', 2, '5-5', 2,'4-5', 2, length=1 / 5  },
		dialog = {'1-1', 2, length=1 / 5 },
		attack = {'2-2',2, length=1 / 5 },
		
        jump = {'3-3', 2, length=0.5 }
    }
}