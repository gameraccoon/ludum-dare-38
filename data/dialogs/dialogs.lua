return {
	level1_npc_3_sw = {
		{
			repeatable = false,
			steps = {
				{
					caption = "Kitten",
					color = {255, 106, 7, 255},
					text = "Where am I? What is this place?"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Hello! Welcome to the Orb Kingdom"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Who are you? Let's get acquainted"
				},
				{
					caption = "Kitten",
					color = {255, 106, 7, 255},
					text = "I'm a kitten"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Kitten?\nNever heard about such creatures"
				},
				{
					caption = "Horned",
					color = {52, 33, 22, 255},
					text = "Sure it came here from another world"
				},
				{
					caption = "Horned",
					color = {52, 33, 22, 255},
					text = "Use keys 'left' and 'right' to move\nPress 'E' to talk"
				},
				{
					caption = "Horned",
					color = {52, 33, 22, 255},
					text = "It's how we do it\nin our kingdom"
				},
			}
		},
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Such an interesting creature!"
				},
			}
		}
	},
	level1_npc_9_baran = {
		{
			repeatable = false,
			steps = {
				{
					caption = "Horned",
					color = {52, 33, 22, 255},
					text = "Are you a newcomer?"
				},
				{
					caption = "Horned",
					color = {52, 33, 22, 255},
					text = "I'm sure our king is waiting for you"
				},
				{
					caption = "Horned",
					color = {52, 33, 22, 255},
					text = "He is always glad to new citizens"
				}
			}
		},
		{
			repeatable = true,
			steps = {
				{
					caption = "Horned",
					color = {52, 33, 22, 255},
					text = "The king is a very wise ruler"
				}
			}
		}
	},
	level1_npc_4_mp = {
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "A wicked wizard wants to\nseize power in the kingdom"
				}
			}
		}
	},
	level1_npc_5_mrchery = {
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "You can press 'Space' to jump"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "It's good for your health\nto jump sometimes"
				}
			}
		}
	},
	level1_npc_12_mutniy = {
		{
			repeatable = false,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "These particles are whirling in the air\nYou can control them"
				},
				{
					caption = "Kitten",
					color = {255, 106, 7, 255},
					text = "It's some kind of magic!\nI'm like a wizard now"
				}
			}
		},
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Just use your mouse\nAren't you a cat?"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Hold left mouse button\nand catch these particles"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "These magic creatures can make\nchanges to this world"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "When you hold the button\nsome silhouettes appears"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Try to activate that silhouettes\nusing the particles"
				}
			}
		}
	},
	level1_npc_13_lemur = {
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "I heard the court magician can transfer\nthings from one world to another"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Maybe he can help you return home"
				}
			}
		}
	},
	level1_npc_11_osminog = {
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Maybe the wicked wizard moved you here"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "He does very bad things here"
				}
			}
		}
	},
	level1_npc_7_zm = {
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "May be you can help us\nto defeat the wizard?"
				}
			}
		}
	},
	level1_npc_10_koza = {
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Do you like this place?"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Do you really want to return?"
				}
			}
		}
	},
	level1_npc_8_taksa = {
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Help us to get rid of the evil wizard!"
				}
			}
		}
	},
	
	level1_man4 = {
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "You'd better to find a way around\nthis wall to reach king's castle"
				}
			}
		}
	},
	level1_man5 = {
		{
			repeatable = false,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Do you wanna be my kitten?"
				}
			}
		},
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "You're so cute"
				}
			}
		}
	},
	level1_man3 = {
		{
			repeatable = false,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Am I inside a snow globe?\nA little orb?"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "That's crazy"
				}
			}
		},
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Ha ha!"
				}
			}
		}
	},
	level1_man2 = {
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Once I was tightened to\nthis place just like you"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "Now I just live here"
				}
			}
		}
	},
	level1_man6 = {
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "The entrance to the castle\nis right on the west"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "But today there are road works\nSo you should find another way"
				}
			}
		}
	},
	level1_man1 = {
		{
			repeatable = true,
			steps = {
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "It's always nice weather here"
				},
				{
					caption = "Citizen",
					color = {0, 0, 0, 255},
					text = "But always the same"
				}
			}
		}
	},
    intro_cat = {
		{
			repeatable = false,
			steps = {
				{
					caption = "Kitten",
					color = {255, 106, 7, 255},
                    image = "intro_cat1.png",
                    pos = "bottom",
					text = "...My mistress is away for so long."
				},
                {
					caption = "Kitten",
					color = {255, 106, 7, 255},
					text = "Let's do something, I'm bored."
				},
                {
					caption = "Kitten",
					color = {255, 106, 7, 255},
					text = "Oh, I think I'll get on the table!"
				},
				{
					caption = "Kitten",
					color = {255, 106, 7, 255},
                    image = "intro_cat2.png",
					text = "Wow, what's the thing?"
				},
                {
					caption = "Kitten",
					color = {255, 106, 7, 255},
                    image = "intro_cat3.png",
                    sound = "sphere",
                    pos = "hide",
					text = "..."
				},
                {
					caption = "Kitten",
					color = {255, 106, 7, 255},
                    image = "intro_cat4.png",
                    sound = "cat_into_sphere",
                    pos = "bottom",
					text = "Meeeeoooooow!!!"
				}
                
			}
		}
	},
    level3_frog = {
		{
			repeatable = false,
			steps = {
                {
					caption = "Kitten",
					color = {255, 106, 7, 255},
					text = "Evil wizard, I came to you to fight!"
				},
				{
					caption = "Frog",
					color = {58,181,21, 255},
					text = "Hahaha! You??"
				},
				{
					caption = "Frog",
					color = {58,181,21, 255},
					text = "I'll smash you like a bug!"
				},
				{
					caption = "Frog",
					color = {58,181,21, 255},
					text = "C'mon!"
				}
			}
		}
	},
    king_cat = {
		{
			repeatable = false,
			steps = {
				{
					caption = "King",
					color = {73,184,236, 255},
                    image = "king_1.png",
                    pos = "bottom",
					text = "Who are you? Interesting creature..."
				},
                {
					caption = "Kitten",
					color = {255, 106, 7, 255},
					text = "You too. What a strange face color!"
				},
                {
					caption = "Kitten",
					color = {255, 106, 7, 255},
					text = "I'm a kitten... from another world."
				},
                {
					caption = "King",
					color = {73,184,236, 255},
					text = "Oh, a kitten. Welcome to my kingdom."
				},
                {
					caption = "King",
					color = {73,184,236, 255},
					text = "Feel yourself at home. Enjoy!"
				},
                {
					caption = "Kitten",
					color = {255, 106, 7, 255},
					text = "But I want to return home."
				},
                {
					caption = "King",
					color = {73,184,236, 255},
					text = "Forget it. Impossible."
				},
                {
					caption = "Kitten",
					color = {255, 106, 7, 255},
					text = "But I heard about the king's mage..."
				},
                {
					caption = "King",
					color = {73,184,236, 255},
					text = "The traitor is exiled in the cave!"
				},
                {
					caption = "Kitten",
					color = {255, 106, 7, 255},
					text = "The cave?"
				},
                {
					caption = "King",
					color = {73,184,236, 255},
					text = "Be warned. He's very dangerous."
				},
				{
					caption = "King",
					color = {73,184,236, 255},
                    image = "king_2.png",
					text = "Take this key - it will open the path..."
				},
                {
					caption = "King",
					color = {73,184,236, 255},
					text = "...to the underground part of the Orb"
				}
                
			}
		}
	},
    frog_cat = {
		{
			repeatable = false,
			steps = {
				{
					caption = "Kitten",
					color = {255, 106, 7, 255},
                    image = "frog_1.png",
                    pos = "hide",
                    sound = "track2",
					text = "..."
				},
                {
					caption = "Kitten",
					color = {255, 106, 7, 255},
                    image = "frog_2.png",
                    sound = "cat_hurt",
					text = "..."
				},
                {
					caption = "Kitten",
					color = {255, 106, 7, 255},
                    image = "frog_3.png",
                    sound = "jump",
					text = "..."
				},
				{
					caption = "Kitten",
					color = {255, 106, 7, 255},
                    image = "frog_4.png",
                    sound = "cat_fight",
					text = "..."
				},
                {
					caption = "Kitten",
					color = {255, 106, 7, 255},
                    image = "frog_5.png",
                    sound = "magic2",
					text = "..."
				},
                {
					caption = "Kitten",
					color = {255, 106, 7, 255},
                    image = "frog_6.png",
                    sound = "frog",
					text = "..."
				}
                
			}
		}
	},
    credits = {
	{
        repeatable = false,
			steps = {
                {
					caption = "Art",
					color = {255, 106, 7, 255},
                    image = "log2.png",
                    pos = "top",
                    sound = "track1",
					text = "Egor Iv, Anntenna"
				},
                {
					caption = "Audio",
					color = {255, 106, 7, 255},
                    image = "frog_4.png",
                    pos = "bottom",
					text = "Joshua McLean, Nudsmachado"
				},
                {
					caption = "Code",
					color = {255, 106, 7, 255},
                    image = "frog_5.png",
                    pos = "top",
					text = "Pavel Grebnev, IgorsGames"
				},
                {
					caption = "Levels",
					color = {255, 106, 7, 255},
                    image = "king_2.png",
                    pos = "bottom",
					text = "IgorsGames, Egor Iv, Anntenna"
				},
				{
					caption = "Script",
					color = {255, 106, 7, 255},
                    image = "king_1.png",
                    pos = "top",
					text = "Anntenna"
				},
                {
					caption = "Font",
					color = {255, 106, 7, 255},
                    image = "frog_1.png",
                    pos = "bottom",
					text = "Press Start 2P by codeman38"
				},
                {
					caption = "Engine",
					color = {255, 106, 7, 255},
                    image = "frog_2.png",
                    pos = "top",
					text = "Love2D + anim8 library by kikito"
				},
                {
					caption = "",
					color = {255, 106, 7, 255},
                    image = "intro_cat2.png",
                    pos = "bottom",
					text = "No kittens were harmed."
				},
                {
					caption = "Kitten :3",
					color = {255, 106, 7, 255},
                    image = "intro_cat1.png",
                    sound = "cat_fall_void",
                    pos = "top",
					text = "Ludum Dare 38 jam. Thanks for playing!"
				}
                
			}
		}
    }
}