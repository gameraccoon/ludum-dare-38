-----------------------------------
-- This file contains configurations of the game
-----------------------------------

love.filesystem.setRequirePath("src/?.lua")

require "utils.Tests"
require "utils.Debug"
require "core.World"
require "core.Game"
require "core.SoundManager"
require "core.Point"
require "core.Camera"

local gameName = "Catsorbed"
local gameId = "ludum_dare_catsorbed"
local canvasSize = MakePoint(400, 300)
local currentUpscale = 1

function SetUpscale(upscale)
	local windowSize = canvasSize * upscale
	love.window.setMode(windowSize.x, windowSize.y)
	currentUpscale = upscale
end

function GetUpscale()
	return currentUpscale
end

SetUpscale(3)
love.filesystem.setIdentity(gameId)
love.window.setTitle(gameName)

love.graphics.setDefaultFilter("nearest", "nearest", 0)

local game = nil

soundManager = nil

function love.load()

	soundManager = MakeSoundManager()
	soundManager:LoadAll()

	SetDebugMode(true)
	if IsDebugMode() then
		RunAllTests()
	end
	game = MakeGame(canvasSize)
	game:Init()

	game:StartGame()
end

function love.update(dt)
	game:Update(dt)
end

function love.draw()

	local camera = game:GetMainCamera()
	if camera then
		camera:Draw()
	else
		warning("camera is nil")
	end
end
